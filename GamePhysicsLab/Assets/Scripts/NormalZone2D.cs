﻿using UnityEngine;

public class NormalZone2D : MonoBehaviour
{
    public Vector2 surfaceNormal;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Particle")
        {
            Particle2D particle = other.gameObject.GetComponent<Particle2D>();

            Vector2 normal = ForceGenerator2D.GenerateForceNormal(particle.GetMass(), Vector2.up, surfaceNormal);

            particle.ApplyForce(normal);
        }
    }
}
