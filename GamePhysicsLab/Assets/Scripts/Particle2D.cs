﻿using UnityEngine;

public class Particle2D : MonoBehaviour
{
    [Header("Linear Motion")]

    public Vector2 position;

    public Vector2 velocity;

    public Vector2 acceleration;

    [Header("Angular Motion")]

    public float rotation;

    public float angularVelocity;

    public float angularAcceleration;

    [Header("Forces")]

    [Range(0f, 25f)] public float initialMass = 1f;

    // (Step 1)

    private float mass;

    private float inverseMass;

    // (Step 2)

    public Vector2 cumulativeForce;

    private void Start()
    {
        position = gameObject.transform.position;
        rotation = gameObject.transform.rotation.z;

        SetMass(initialMass);
    }

    public void SetMass(float newMass)
    {
        mass = Mathf.Max(0f, newMass);
        if (mass == 0f)
            inverseMass = 0f;
        else
            inverseMass = 1f / mass;
    }

    public float GetMass()
    {
        return mass;
    }

    public float GetInverseMass()
    {
        return inverseMass;
    }

    private void FixedUpdate()
    {
        UpdatePosition(Time.fixedDeltaTime);
        UpdateRotation(Time.fixedDeltaTime);

        UpdateAcceleration();

        gameObject.transform.position = position;
        gameObject.transform.rotation = Quaternion.Euler(0f, 0f, rotation);
    }

    // (Step 2)

    public void ApplyForce(Vector2 force)
    {
        // D'Alembert's principle allows us to treat all forces as one force
        cumulativeForce += force;
    }

    private void UpdateAcceleration()
    {
        // calculate the acceleration using Newton's Second Law
        acceleration = inverseMass * cumulativeForce;

        // reset forces at the end of the frame
        cumulativeForce = Vector2.zero;
    }

    private void UpdatePosition(float dt) // kinematic
    {
        position += (velocity * dt) + (0.5f * acceleration * dt * dt);
        velocity += acceleration * dt;
    }

    private void UpdateRotation(float dt) // kinematic
    {
        rotation += (angularVelocity * dt) + (0.5f * angularAcceleration * dt * dt);
        angularVelocity += angularAcceleration * dt;
    }
}