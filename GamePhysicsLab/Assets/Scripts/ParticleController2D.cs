﻿using UnityEngine;

public class ParticleController2D : MonoBehaviour
{
    // (Step 4)

    public enum ControlMode
    {
        Manual, Oscillation, Gravity
    }

    public ControlMode mode;

    public bool invertOscillation;

    public bool invertGravity;

    private Particle2D particle;

    private const float gravity = 0.988f;

    private void Start()
    {
        particle = gameObject.GetComponent<Particle2D>();
    }

    private void Update()
    {
        switch (mode)
        {
            case ControlMode.Manual:
                GetManualAcceleration(); // WASD controls for linear, QE for rotational
                break;

            case ControlMode.Oscillation:
                Oscillate(); // wiggle back-and-forth or up-and-down
                break;

            case ControlMode.Gravity:
                ApplyGravity(); // fall downward 
                break;

            default:
                break;
        }      
    }

    public void GetManualAcceleration()
    {
        if (Input.GetKey(KeyCode.W))
        {
            particle.acceleration.y += 0.01f;
        }

        if (Input.GetKey(KeyCode.S))
        {
            particle.acceleration.y -= 0.01f;
        }

        if (Input.GetKey(KeyCode.A))
        {
            particle.acceleration.x -= 0.01f;
        }

        if (Input.GetKey(KeyCode.D))
        {
            particle.acceleration.x += 0.01f;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            particle.angularAcceleration += 0.5f;
        }

        if (Input.GetKey(KeyCode.E))
        {
            particle.angularAcceleration -= 0.5f;
        }

        if (Input.GetKeyDown(KeyCode.Space)) // reset all velocity and acceleration
        {
            particle.angularAcceleration = 0f;
            particle.acceleration = Vector2.zero;
            particle.angularVelocity = 0f;
            particle.velocity = Vector2.zero;
        }
    }

    public void Oscillate()
    {
        if (!invertOscillation)
        {
            particle.acceleration = new Vector2(0f, -2.5f * Mathf.Sin(Time.fixedTime));
        }
        else
        {
            particle.acceleration = new Vector2(0f, 2.5f * Mathf.Sin(Time.fixedTime));
        }
    }

    public void ApplyGravity()
    {
        if (!invertGravity)
        {
            particle.acceleration = new Vector2(particle.acceleration.x, -gravity);
        }
        else
        {
            particle.acceleration = new Vector2(particle.acceleration.x, gravity);
        }       
    }
}