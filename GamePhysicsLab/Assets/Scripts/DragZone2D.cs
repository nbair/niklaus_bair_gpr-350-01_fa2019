﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragZone2D : MonoBehaviour
{
    public Vector2 fluidVelocity;

    public float fluidDensity;

    public float dragCoefficient;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Particle")
        {
            Particle2D particle = other.gameObject.GetComponent<Particle2D>();

            Vector2 drag = ForceGenerator2D.GenerateForceDrag(particle.velocity, fluidVelocity, fluidDensity, other.bounds.size.x * other.bounds.size.z, dragCoefficient);

            particle.ApplyForce(drag);
        }
    }
}