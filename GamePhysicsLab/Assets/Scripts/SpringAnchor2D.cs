﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringAnchor2D : MonoBehaviour
{
    public float restingLength;

    public float k;

    public float damping;

    public Particle2D particle;

    public bool drawDebugSpring = true;

    private void FixedUpdate()
    {
        Vector2 spring = ForceGenerator2D.GenerateForceSpring(particle.position, gameObject.transform.position, restingLength, k);
        particle.ApplyForce(damping * spring);
    }

    private void Update()
    {
        if (drawDebugSpring)
            Debug.DrawLine(gameObject.transform.position, particle.position, Color.red, Time.deltaTime);
    }
}