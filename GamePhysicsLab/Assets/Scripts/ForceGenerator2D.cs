﻿using UnityEngine;

public class ForceGenerator2D
{
    // (Step 3)

    private const float g = 9.8f;

    private static Vector2 Vector2Project(Vector2 a, Vector2 b)
    {
        return Vector2.Dot(a, b) * (1 / (b.magnitude * b.magnitude)) * b;
    }

    public static Vector2 GenerateForceGravity(float particleMass, Vector2 worldUp)
    {
        return particleMass * g * -worldUp;
    }

    public static Vector2 GenerateForceNormal(Vector2 forceOfGravity, Vector2 surfaceNormal) // if we already have gravity
    {
        return forceOfGravity.magnitude * -Vector2Project(forceOfGravity, surfaceNormal).normalized;
    }

    public static Vector2 GenerateForceNormal(float particleMass, Vector2 worldUp, Vector2 surfaceNormal) // if we need to recalculate gravity
    {
        return GenerateForceNormal(particleMass * g * -worldUp, surfaceNormal);
    }

    public static Vector2 GenerateForceSliding(Vector2 forceOfGravity, Vector2 normalForce)
    {
        return forceOfGravity + normalForce;
    }

    public static Vector2 GenerateForceStaticFriction(Vector2 normalForce, Vector2 opposingForce, float staticFrictionCoefficient)
    {
        float max = staticFrictionCoefficient * normalForce.magnitude;
        if (opposingForce.magnitude < max)
        {
            return -opposingForce;
        }
        else
        {
            return -staticFrictionCoefficient * normalForce;
        }
    }

    public static Vector2 GenerateForceKineticFriction(Vector2 normalForce, Vector2 particleVelocity, float kinematicFrictionCoefficient)
    {
        return -kinematicFrictionCoefficient * normalForce.magnitude * particleVelocity.normalized;
    }

    public static Vector2 GenerateForceDrag(Vector2 particleVelocity, Vector2 fluidVelocity, float fluidDensity, float objectAreaCrossSection, float objectDragCoefficient)
    {
        Vector2 u = particleVelocity - fluidVelocity;
        return 0.5f * objectAreaCrossSection * objectDragCoefficient * fluidDensity * u * u;
    }

    public static Vector2 GenerateForceSpring(Vector2 particlePosition, Vector2 anchorPosition, float springRestingLength, float springStiffnessCoefficient)
    {
        Vector2 direction = particlePosition - anchorPosition;
        float displacement = direction.magnitude - springRestingLength;

        return -springStiffnessCoefficient * displacement * direction.normalized;
    }
}