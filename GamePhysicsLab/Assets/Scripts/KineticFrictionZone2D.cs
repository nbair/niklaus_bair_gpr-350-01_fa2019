﻿using UnityEngine;

public class KineticFrictionZone2D : MonoBehaviour
{
    public Vector2 surfaceNormal;

    public float kineticFrictionCoefficent;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Particle")
        {
            Particle2D particle = other.gameObject.GetComponent<Particle2D>();

            Vector2 normal = ForceGenerator2D.GenerateForceNormal(particle.GetMass(), Vector2.up, surfaceNormal);
            Vector2 kineticFriction = ForceGenerator2D.GenerateForceKineticFriction(normal, particle.velocity, kineticFrictionCoefficent);

            particle.ApplyForce(kineticFriction);
        }
    }
}