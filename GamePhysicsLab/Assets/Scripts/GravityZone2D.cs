﻿using UnityEngine;

public class GravityZone2D : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Particle")
        {
            Particle2D particle = other.gameObject.GetComponent<Particle2D>();

            Vector2 gravity = ForceGenerator2D.GenerateForceGravity(particle.GetMass(), Vector2.up);

            particle.ApplyForce(gravity);
        }
    }
}